//
//  ViewController.m
//  ScrollView
//
//  Created by yoseop on 2014. 10. 16..
//  Copyright (c) 2014년 yoseop. All rights reserved.
//

#import "ViewController.h"

#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"

@interface ViewController ()<UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong) FirstViewController *firstViewController;
@property (strong) SecondViewController *secondViewController;
@property (strong) ThirdViewController *thirdViewController;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadviewController];
}

- (void)viewWillAppear:(BOOL)animated
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    // 몇번째 페이지를 상단에 띄울것인가. 여기서는 2로 선택함.
    CGPoint scrollPoint = CGPointMake(screenWidth * 2,0);
    
    self.scrollView.contentOffset = scrollPoint;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadviewController
{
    self.firstViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstViewController"];
    self.secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    self.thirdViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ThirdViewController"];
    
    [self setScrollViewFrame];
    [self setChildViewControllersViewFrame];
    [self addChildViewControllers];
}

- (void)setScrollViewFrame
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHight = [UIScreen mainScreen].bounds.size.height;
    CGRect frame = CGRectMake(0, 0, screenWidth, screenHight);
    self.scrollView.frame = frame;
}

- (void)setChildViewControllersViewFrame
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHight = [UIScreen mainScreen].bounds.size.height;
    
    CGFloat firstViewScreenOriginOfX = screenWidth * 0;
    CGFloat secondViewScreenOriginOfX = screenWidth * 1;
    CGFloat thirdViewScreenOriginOfX = screenWidth * 2;
    
    self.firstViewController.view.frame = CGRectMake(firstViewScreenOriginOfX, 0, screenWidth, screenHight);
    self.secondViewController.view.frame = CGRectMake(secondViewScreenOriginOfX, 0, screenWidth, screenHight);
    self.thirdViewController.view.frame = CGRectMake(thirdViewScreenOriginOfX, 0, screenWidth, screenHight);
}

- (void)addChildViewControllers
{
    [self addChildViewController:self.firstViewController];
    [self addChildViewController:self.secondViewController];
    [self addChildViewController:self.thirdViewController];
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHight = [UIScreen mainScreen].bounds.size.height;
    
    self.scrollView.contentSize = CGSizeMake(screenWidth * 3, screenHight);
    
    [self.scrollView addSubview:self.firstViewController.view];
    [self.scrollView addSubview:self.secondViewController.view];
    [self.scrollView addSubview:self.thirdViewController.view];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    NSLog(@"_page : %d",page);

    self.scrollView.scrollEnabled = YES;

}

@end
